def search():
    return {
        "currency": "USD",
        "decimal": "11.1",
        "path": "/foo/bar/baz",
        "timestamp": "2018-10-02T10:00:00-05:00",
        "url": "http://example.com",
        "uuid": "12345678-1234-1234-1234-123456789abc",
    }
